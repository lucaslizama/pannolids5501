@extends('layouts.app')

@section('title', '| Categorias')

@section('content')

<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-key"></i> Categorias
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Categorias</th>
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($categorias as $categoria)
                <tr>

                    <td>{{ $categoria->nombre }}</td>
                    <td>
                    <a href="{{ URL::to('categorias/'.$categoria->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['categorias.destroy', $categoria->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    <a href="{{ URL::to('categorias/create') }}" class="btn btn-success">Agregar Categoria</a>

</div>

@endsection