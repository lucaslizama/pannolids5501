@extends('layouts.app')

@section('title', '| Editar Categoria')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Editar {{$categoria->nombre}}</h1>
    <br>
    {{ Form::model($categoria, array('route' => array('categorias.update', $categoria->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre Categoria') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
    </div>
    <br>
    {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection