@extends('layouts.app')

@section('title', '| Crear Marca')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Agregar Marca</h1>
    <br>

    {{ Form::open(array('url' => 'marcas')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', '', array('class' => 'form-control')) }}
    </div>
    <br>
    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection