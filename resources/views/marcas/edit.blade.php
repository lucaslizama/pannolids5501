@extends('layouts.app')

@section('title', '| Editar Marcas')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Editar {{$marca->nombre}}</h1>
    <br>
    {{ Form::model($marca, array('route' => array('marcas.update', $marca->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre Marca') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
    </div>
    <br>
    {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection