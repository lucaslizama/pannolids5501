@extends('layouts.app')

@section('title', '| Marcas')

@section('content')

<div class="col-lg-10 col-lg-offset-1">
    <h1><i class="fa fa-key"></i> Marcas
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Marcas</th>
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($marcas as $marca)
                <tr>

                    <td>{{ $marca->nombre }}</td>
                    <td>
                    <a href="{{ URL::to('marcas/'.$marca->id.'/edit') }}" class="btn btn-info pull-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['marcas.destroy', $marca->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>

    <a href="{{ URL::to('marcas/create') }}" class="btn btn-success">Agregar Marca</a>

</div>

@endsection