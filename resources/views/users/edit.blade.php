{{-- \resources\views\users\edit.blade.php --}}

@extends('layouts.app')

@section('title', '| Edit User')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-user-plus'></i> Edit {{$user->nombre . ' ' . $user->apellido}}</h1>
    <hr>

    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

    <div class="form-group">
        {{ Form::label('rut', 'Rut') }}
        {{ Form::text('rut', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('apellido', 'Apellido') }}
        {{ Form::text('apellido', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('escuela', 'Escuela') }}
        <select class="form-control" name="id_escuela" id="escuela" required>
            <option value="0">Elija Escuela</option>
            @foreach(App\Escuela::all() as $escuela)
                <!-- @if(Auth::user()->escuela->id == $escuela->id)
                    <option value="{{ $escuela->id }}" selected>{{ $escuela->nombre }}</option>
                @else -->
                    <option value="{{ $escuela->id }}">{{ $escuela->nombre }}</option>
                <!-- @endif -->
            @endforeach
        </select>
    </div>

    <div class="form-group">
        {{ Form::label('domicilio', 'Domicilio') }}
        {{ Form::text('domicilio', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('telefono', 'Telefono') }}
        {{ Form::text('telefono', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('correo', 'Correo') }}
        {{ Form::email('correo', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Give Role</b></h5>

    <div class='form-group'>
        @foreach ($roles as $role)
            {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

        @endforeach
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}

    </div>

    <div class="form-group">
        {{ Form::label('password', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

    </div>

    {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection