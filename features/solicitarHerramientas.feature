Feature: Solicitar Herramientas
Como Alumno solicito herramientas 
para realizar un laboratorio

Scenario: Alumno cumple condiciones
  Then que no tengo prestamos pendientes
  When solicito el uso de un taladro
  Then se me autoriza el prestamo

Scenario: Alumno no cumple condiciones
  Given que tengo un prestamo pendiente
  When solitito el uso de un taladro
  Then no se autoriza el prestamo