<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'rut' => 'rut|required|string|min:7|unique:users',
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'domicilio' => 'required|string',
            'telefono' => 'required|numeric|min:9',
            'id_escuela' => 'required|numeric',
            'correo' => 'required|email',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'rut' => $data['rut'],
            'nombre' => $data['name'],
            'apellido' => $data['apellido'],
            'domicilio' => $data['domicilio'],
            'telefono' => $data['domicilio'],
            'id_escuela' => $data['id_escuela'],
            'correo' => $data['correo'],
            'password' => $data['password'],
        ]);
    }
}
