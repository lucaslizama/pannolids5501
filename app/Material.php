<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'materiales';

    protected $fillable = [
        'id_categoria', 'id_marca', 
        'id_stock', 'nombre', 'observacion',
        'imagen'
    ];
}
