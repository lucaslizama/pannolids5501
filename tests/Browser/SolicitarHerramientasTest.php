<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SolicitarHerramientasTest extends DuskTestCase
{

    /**
     * @var Browser
     */
    protected $browser;

    public function testAlumnoCumpleCondiciones()
    {

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->thenQueNoTengoPrestamosPendientes();
            $this->whenSolicitoElUsoDeUnTaladro();
            $this->thenSeMeAutorizaElPrestamo();
        });
    }


    protected function thenQueNoTengoPrestamosPendientes()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }

    protected function whenSolicitoElUsoDeUnTaladro()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }

    protected function thenSeMeAutorizaElPrestamo()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }

    public function testAlumnoNoCumpleCondiciones()
    {

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->givenQueTengoUnPrestamoPendiente();
            $this->whenSolititoElUsoDeUnTaladro();
            $this->thenNoSeAutorizaElPrestamo();
        });
    }


    protected function givenQueTengoUnPrestamoPendiente()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }

    protected function whenSolititoElUsoDeUnTaladro()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }

    protected function thenNoSeAutorizaElPrestamo()
    {
        $this->markTestIncomplete('Time to code');

        $this->browser->visit('/');
        //and in the next step
        //$this->browser->assertSee('Laravel');
    }


}
