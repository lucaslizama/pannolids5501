<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SolicitarHerramientasTest extends TestCase
{


    public function testAlumnoCumpleCondiciones() {

        $this->thenQueNoTengoPrestamosPendientes();
        $this->whenSolicitoElUsoDeUnTaladro();
        $this->thenSeMeAutorizaElPrestamo();
    }


    protected function thenQueNoTengoPrestamosPendientes() {

        $this->markTestIncomplete('Time to code');

    }

    protected function whenSolicitoElUsoDeUnTaladro() {

        $this->markTestIncomplete('Time to code');

    }

    protected function thenSeMeAutorizaElPrestamo() {

        $this->markTestIncomplete('Time to code');

    }

    public function testAlumnoNoCumpleCondiciones() {

        $this->givenQueTengoUnPrestamoPendiente();
        $this->whenSolititoElUsoDeUnTaladro();
        $this->thenNoSeAutorizaElPrestamo();
    }


    protected function givenQueTengoUnPrestamoPendiente() {

        $this->markTestIncomplete('Time to code');

    }

    protected function whenSolititoElUsoDeUnTaladro() {

        $this->markTestIncomplete('Time to code');

    }

    protected function thenNoSeAutorizaElPrestamo() {

        $this->markTestIncomplete('Time to code');

    }


}
