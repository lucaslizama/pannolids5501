<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escuelas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->timestamps();
        });

        DB::table('escuelas')->insert([
            [
                'nombre' => 'Administración y Negocios',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Comunicación',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Construccion',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Diseño',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Informática y Telecomunicaciones',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Ingeniería',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Recursos Naturales',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Salud',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
            [
                'nombre' => 'Turismo',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escuelas');
    }
}
