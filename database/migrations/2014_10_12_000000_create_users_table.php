<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rut')->unique();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('domicilio');
            $table->string('telefono');
            $table->unsignedInteger('id_escuela')->nullable();
            $table->string('correo')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_escuela')->references('id')->on('escuelas');
        });

        DB::table('users')->insert([
            [
                'rut' => '18464695-1',
                'nombre' => 'Lucas',
                'apellido' => 'Lizama',
                'domicilio' => 'Los Militares 5200 depto 34',
                'telefono' => '123456789',
                'correo' => 'lucaslizama3@hotmail.com',
                'password' => bcrypt('123456'),
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
